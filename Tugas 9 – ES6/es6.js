//1. Mengubah fungsi menjadi fungsi arrow

console.log("JAWABAN No. 1");
console.log("");

// const golden = function goldenFunction(){
//   console.log("this is golden!!")
// }

const golden = () => {
  console.log("this is golden!!")
}
 
golden()

console.log("");

//2. Sederhanakan menjadi Object literal di ES6

console.log("JAWABAN No. 2");
console.log("");

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//       return 
//     }
//   }
// }
 
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

console.log("");

//3. Destructuring

console.log("JAWABAN No. 3");
console.log("");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// const { firstName, lastName } = studentName;

const { firstName, lastName,destination, occupation } = newObject;

console.log(firstName, lastName, destination, occupation);

console.log("");

//4. Array Spreading

console.log("JAWABAN No. 4");
console.log("");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//const combined = west.concat(east)

//Driver Code

let combined = [...west, ...east];

console.log(combined);

console.log("");

//5. Template Literals

console.log("JAWABAN No. 5");
console.log("");

const planet = "earth";
const view = "glass";

// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
// Driver Code
const before = `Lorem  ${view}   dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

console.log(before) 


