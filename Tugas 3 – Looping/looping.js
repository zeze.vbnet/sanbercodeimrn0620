/*No. 1 Looping While*/ 

/* LOOPING PERTAMA */
console.log("LOOPING PERTAMA");

var iterasi = 2;

while(iterasi <= 20) { 
  console.log(iterasi + " - I love coding"); 
  iterasi = iterasi + 2; 
}

console.log(' ');

/* LOOPING KEDUA */
console.log("LOOPING KEDUA");

var iterasi = 20;

while(iterasi > 0) { 
  console.log(iterasi + " - I will become a mobile developer "); 
  iterasi = iterasi - 2; 
}

console.log(' ');

/*No. 2 Looping menggunakan*/
for(var i = 1; i <= 20; i++) {
  
  if ((i%3 == 0) && (i%2 != 0)) {
    console.log (i + " - I Love Coding");
  } else {
    if (i%2 == 0) {
      console.log(i + " - Berkualitas");
    } else {
      console.log(i + " - Santai");
    };

  };

} 

console.log(' ');

/*No. 3 Membuat Persegi Panjang*/

for (var i = 1; i <= 4; i++){

  var str = "";

  for (var j = 1; j <= 8; j++){

    str = str + "#";

  };

  console.log(str);

};

console.log(' ');

/*No. 4 Membuat Tangga*/

for (var i = 1; i <= 7; i++){

  var str = "";

  for (var j = 1; j <= i; j++){

    str = str + "#";

  };

  console.log(str);

};

console.log(' ');

/*No. 5 Membuat Papan Catur*/

for (var i = 1; i <= 8; i++){
  
  // if (i%2 == 0) {
  //   var str = "#";
  // } else {
  //   var str = " ";
  // };

  var str = "";
  
  for (var j = 1; j <= 8; j++){
   
    if (i%2 == 0) {
      if (j%2 == 0) {
        str = str + " ";
      } else {
        str = str + "#";
      };
    } else {
      if (j%2 == 0) {
        str = str + "#";
      } else {
        str = str + " ";
      };
    };

  };

  console.log(str);

};