// Soal No. 1 (Range)
console.log('JAWABAN SOAL NO 1');
console.log('');

function range(startNum, finishNum) {
  var arrNum = []

  if ((startNum ==  undefined ) || (finishNum ==  undefined )){
    return -1;
  };

  if (startNum < finishNum){
    for(var i = 0; i<= finishNum - startNum; i++){
      arrNum[i] = startNum + i;
    }
  }else{
    for(var i = 0; i<= startNum - finishNum; i++){
      arrNum[i] = startNum - i;
    }
  };

  return arrNum;
};

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 

console.log('');

// Soal No. 2 (Range with Step)
console.log('JAWABAN SOAL NO 2');
console.log('');

function rangeWithStep(startNum, finishNum, step) {
  var arrNum = [];
  var i;

  if ((startNum ==  undefined ) || (finishNum ==  undefined ) || (step == undefined)){
    return -1;
  };

  if (startNum < finishNum){
    for(var i = 0; i<= finishNum - startNum; i += step){
      arrNum.push(startNum + i);
    }
  }else{
    for(var i = 0; i<= startNum - finishNum; i += step){
      arrNum.push(startNum - i);
    }
  };

  return arrNum;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)
console.log('JAWABAN SOAL NO 3');
console.log('');

function sum(startNum, finishNum = startNum, step = 1) {
  var arrNum = [];
  var i;
  var sum = 0;

  if ((startNum ==  undefined ) || (finishNum ==  undefined )){
    return 0;
  };

  if (startNum < finishNum){
    for(var i = 0; i<= finishNum - startNum; i += step){
      arrNum.push(startNum + i);
      sum += arrNum[arrNum.length - 1];
    }
  }else{
    for(var i = 0; i<= startNum - finishNum; i += step){
      arrNum.push(startNum - i);
      sum += arrNum[arrNum.length - 1];
    }
  };

  return sum;
};

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 

// Soal No. 4 (Array Multidimensi)
console.log('JAWABAN SOAL NO 4');
console.log('');

//contoh input
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(arrData) {
  var str =''

  for (i = 0; i <= arrData.length - 1; i++){

    str += "Nomor ID: " + arrData[i][0] + "\r\n" +
     "Nama Lengkap: " + arrData[i][1] + "\r\n" +
     "TTL: " +  arrData[i][2] + " " + arrData[i][3] + "\r\n" + 
     "Hobi: " + arrData[i][4] + "\r\n" + "\r\n";

  };

  return str;

};

console.log(dataHandling(input));

// Soal No. 5 (Balik Kata)
console.log('JAWABAN SOAL NO 5');
console.log('');

function balikKata(data){
  var str = ""

  for (i=data.length - 1; i >= 0; i--){
    str += data[i];
  };

  return str;
};

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

console.log(" ");

// Soal No. 6 (Metode Array)
console.log('JAWABAN SOAL NO 6');
console.log('');

function dataHandling2(arrData){
  var arrTgl = [];
  var arrTglSort = [];

  arrData.splice(1,4,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro");

  console.log(arrData);

  arrTgl = arrData[3].split('/');
  
  switch (arrTgl[1]) {
    case "01":
      console.log("Januari");
      break;
    case "02":
      console.log("Februari");
      break;
    case "03":
      console.log("Maret");
      break;
    case "04":
      console.log("April");
      break;
    case "05":
      console.log("Mei");
      break;
    case "06":
      console.log("Juni");
      break;
    case "07":
      console.log("Juli");
      break;
    case "08":
      console.log("Agustus");
      break;
    case "09":
      console.log("September");
      break;
    case "10":
      console.log("Oktober");
      break;
    case "11":
      console.log("November");
      break;
    case "12":
      console.log("Desember");
      break;
    default:
      break;
  };

  arrTglSort = arrTgl.slice(0);

  arrTglSort.sort(function(a, b){return b-a});

  console.log(arrTglSort);

  console.log(arrTgl.join("-"));

  var strNama = arrData.slice(1,2).toString();

  console.log(strNama.substring(0,15));
};

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

dataHandling2(input);