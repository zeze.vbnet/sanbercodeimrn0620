/*If-else*/

var nama = "Junaedi";
var peran = "Werewolf";

if ( nama == "" ) {
  console.log("Nama harus diisi!");

} else {

  if (peran == "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
  } else {

    if (peran == "Penyihir") {
      console.log("Selamat datang di Dunia Werewolf, " + nama);
      console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "Guard") {
      console.log("Selamat datang di Dunia Werewolf, " + nama);
      console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else {
      console.log("Selamat datang di Dunia Werewolf, " + nama);
      console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } 

  }
  
}

console.log(" ");

/* Switch Case */

var hari = 21; 
var bulan = 1; 
var tahun = 1945;

var int_hari = 0;
var str_bulan = "";
var int_tahun = 0;

if ( (hari >= 1) && (hari <= 31) ) {
  int_hari = hari;
} else {
  int_hari = 0;
}

switch(bulan) {
  case 1:{ str_bulan = "Januari"; break; }
  case 2:{ str_bulan = "Februari"; break; }
  case 3:   { str_bulan = "Maret"; break; }
  case 4:   { str_bulan = "April"; break; }
  case 5: { str_bulan = "Mei"; break }
  case 6: { str_bulan = "Juni"; break; }
  case 7: {str_bulan = "Juli"; break; }
  case 8: {str_bulan = "Agustus"; break; }
  case 9: {str_bulan = "September"; break; }
  case 10: {str_bulan = "Oktober"; break; }
  case 11: {str_bulan = "November"; break; }
  case 12: {str_bulan = "Desember"; break; }
  default:  {str_bulan = ""; }};

  if (tahun >= 1900 && tahun <= 2200) {
    int_tahun = tahun;
  } else {
    int_tahun = 0;
  }

  if(int_hari == 0){
    console.log("Hari harus berada dalam range 1 - 31");

  } else {

    if(str_bulan == ""){

      console.log("Bulan harus berada dalam range 1 - 12");

    } else {

        if (int_tahun == 0) {

          console.log("Tahun harus berada dalam range 1990 - 2200");

          
        } else {

          console.log("Maka hasil yang akan tampil di console adalah: " + hari + " " + str_bulan + " " + tahun);

        };

    };

  }; 

  

  