//Soal No. 1 (Array to Object)
console.log("JAWABAN No.1");

function arrayToObject(arr) {
  // Code di sini

  let arrPerson = [];

  if(typeof foo === 'undefined'){
    console.log('""');
  };

  for (let i = 0; i < arr.length; i++) {

    var objPerson = new Object ();
    
    objPerson.firstName = arr[i][0];
    objPerson.lastName = arr[i][1];
    objPerson.gender = arr[i][2];

    var now = new Date()
    const thisYear = now.getFullYear() // 2020 (tahun sekarang

    if ((!arr[i][3])||(thisYear < arr[i][3])){
      objPerson.age = "Invalid birth year";
    }else{
      objPerson.age = thisYear - arr[i][3];
    }

    arrPerson.push(objPerson);

  };

  const objHasil = arrPerson.reduce((obj, item) => {
     obj[(item.firstName + " " + item.lastName)] = item;
     return obj;
  }, {});

   let i = 1;

  Object.keys(objHasil).forEach(function(key) {
    
    console.log(i + ". " + key + " : " , objHasil[key]);

    i++;
  
  });

};

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);
/*
//   1. Bruce Banner: { 
//       firstName: "Bruce",
//       lastName: "Banner",
//       gender: "male",
//       age: 45
//   }
//   2. Natasha Romanoff: { 
//       firstName: "Natasha",
//       lastName: "Romanoff",
//       gender: "female".
//       age: "Invalid Birth Year"
//   }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2); 
/*
  1. Tony Stark: { 
      firstName: "Tony",
      lastName: "Stark",
      gender: "male",
      age: 40
  }
  2. Pepper Pots: { 
      firstName: "Pepper",
      lastName: "Pots",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

// Error case 
arrayToObject([]);

console.log("");

//Soal No. 2 (Shopping Time)
console.log("JAWABAN No.2");

function shoppingTime(memberId, money) {
  // you can only write your code here!

  var objSale = new Object ();
  var arrPurchased = [];

  const sale = [{barang : "Sepatu Stacattu",price : 1500000},
  {barang : "Baju Zoro", price : 500000},
  {barang : "Baju H&N", price : 250000},
  {barang : "Sweater Uniklooh", price : 175000},
  {barang : "Casing Handphone", price : 50000}];

  sale.sort((a, b) =>
    parseFloat(b.price) - parseFloat(a.price)
  );

  if(!memberId){
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }else if(money < 50000){
    return "Mohon maaf, uang tidak cukup";
  } else{
    objSale.memberId = memberId;
    objSale.money = money;

    changeMoney = money;

    Object.entries(sale).forEach(([key, value]) => {

      if (changeMoney >= value['price']){
      
        arrPurchased.push(value['barang']);

        changeMoney = changeMoney - value['price'];
      };

    });

    objSale.listPurchased = arrPurchased;

    objSale.changeMoney = changeMoney;

    return objSale;
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("");

//Soal No. 3 (Naik Angkot)
console.log("JAWABAN No.3");

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here

  var arrRute = [];

  Object.entries(arrPenumpang).forEach(([key, value]) => {

    var objRute = new Object ();

    objRute.penumpang = arrPenumpang[key][0];
    objRute.naikDari = arrPenumpang[key][1];
    objRute.tujuan = arrPenumpang[key][2];

    objRute.bayar = (rute.indexOf(arrPenumpang[key][2]) - rute.indexOf(arrPenumpang[key][1])) * 2000;

    arrRute.push(objRute);

  });

  return arrRute;

};
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]